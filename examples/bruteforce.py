#!/usr/bin/env python2
import string
from hashlib import sha256
from pwn import *
from pwnlib.util.iters import mbruteforce

def compute_string(condition, prefix, alphabet, length):
    """
    Returns true for any input where sha256(prefix+guess) digest fulfills the
    condition
    """
    # Function returning True for success
    t = lambda x: sha256(prefix+x).digest().startswith(condition)
    result = mbruteforce(t, alphabet, length=length,
                         method='fixed')
    return prefix+result

if __name__ == "__main__":
    alphabet = string.letters + string.digits
    length = 6
    result = compute_string(b"\x00\x11\x22", "ff", alphabet, length)
    log.info("sha256({}) == {}".format(result, sha256(result).hexdigest()))

