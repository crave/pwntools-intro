BASENAME=slides

.PHONY: default clean compile all
all: compile examples

compile: $(BASENAME).pdf

tex: $(BASENAME).tex

examples: examples/buffer_overflow/main

$(BASENAME).pdf: $(BASENAME).md
	pandoc --highlight-style intense.theme -t beamer $< -o $@

$(BASENAME).tex: $(BASENAME).md
	pandoc -s --highlight-style intense.theme -t latex $< -o $@


clean:
	rm -f $(BASENAME).pdf $(BASENAME).tex
